package shoppingbot.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

public class WebSession {

    private final String USER_AGENT_MOZILLA = "Mozilla/5.0 (Windows NT 5.1; rv:32.0) Gecko/20100101 Firefox/32.0";

    private int readTimeout = 10000;
    private int connectTimeout = 15000;
    private String cookies = ""; // conn.getHeaderField("Set-Cookie");

//    private HttpURLConnection conn = null;
    // private com.example.runescape_pricechecker.CookieManager cm = new
    // com.example.runescape_pricechecker.CookieManager();
    public WebSession() {
        // TODO Auto-generated constructor stub
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public InputStream POST(String Url, String postData) throws IOException {
        InputStream is = null;

        URL url = new URL(Url);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        try {
//            if (conn != null) {
//                cookies = conn.getHeaderField("Set-Cookie");
//            }

            if (cookies != null && cookies.equalsIgnoreCase("") == false) {
                conn.setRequestProperty("Cookie", cookies);
            }

            conn.setReadTimeout(readTimeout);
            /* milliseconds */
            conn.setConnectTimeout(connectTimeout);
            /* milliseconds */
            conn.setDoOutput(true);
            // conn.setChunkedStreamingMode(0);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("User-Agent", USER_AGENT_MOZILLA);
            Writer writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(postData);
            writer.flush();
            writer.close();

            conn.setDoInput(true);

            // Starts the query
            conn.connect();

            // cm.storeCookies(conn);
            is = conn.getInputStream();
            // conn.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return is;

    }

    public InputStream POST(String Url, String postData, String Referer) throws IOException {
        InputStream is = null;

        URL url = new URL(Url);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        try {

            if (cookies != null && cookies.equalsIgnoreCase("") == false) {
                conn.setRequestProperty("Cookie", cookies);
            }

            conn.setReadTimeout(readTimeout);
            /* milliseconds */
            conn.setConnectTimeout(connectTimeout);
            /* milliseconds */
            conn.setDoOutput(true);
            // conn.setChunkedStreamingMode(0);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Referer", Referer);
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            conn.setRequestProperty("User-Agent", USER_AGENT_MOZILLA);
            conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            Writer writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(postData);
            writer.flush();
            writer.close();

            // Starts the query
            conn.connect();

            // cm.storeCookies(conn);
            is = conn.getInputStream();
            // conn.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return is;

    }

    public InputStream GET(String urlString) throws IOException {
        InputStream is = null;

        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        try {

            conn.setRequestProperty("User-Agent", USER_AGENT_MOZILLA);
            conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            conn.setReadTimeout(readTimeout);
            /* milliseconds */
            conn.setConnectTimeout(connectTimeout);
            /* milliseconds */
            conn.setRequestMethod("GET");

            conn.setDoInput(true);

            // Starts the query
            conn.connect();

            // cm.storeCookies(conn);
            is = conn.getInputStream();
            // conn.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return is;
    }

    public InputStream GET(String urlString, String Referer) throws IOException {
        InputStream is = null;

        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        try {

            // cm.setCookies(conn);
            conn.setRequestProperty("User-Agent", USER_AGENT_MOZILLA);
            conn.setReadTimeout(readTimeout);
            /* milliseconds */
            conn.setConnectTimeout(connectTimeout);
            /* milliseconds */
            conn.setRequestProperty("Referer", Referer);

            conn.setRequestMethod("GET");

            conn.setDoInput(true);

            // Starts the query
            conn.connect();

            // cm.setCookies(conn);
            is = conn.getInputStream();
            // conn.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) {
               // conn.disconnect();
            }
        }

        return is;
    }

//	public Drawable DownloadImage(String urlString) throws IOException
//	{
//		InputStream is = null;
//
//		try
//		{
//			URL url = new URL(urlString);
//			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//
//			conn.setRequestProperty("User-Agent",
//					"Mozilla/5.0 (Linux; U; Android 4.2.2; en-us; sdk Build/JB_MR1.1) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
//
//			conn.setReadTimeout(readTimeout); /* milliseconds */
//			conn.setConnectTimeout(connectTimeout); /* milliseconds */
//			conn.setRequestMethod("GET");
//
//			conn.setDoInput(true);
//
//			// Starts the query
//			conn.connect();
//
//			is = conn.getInputStream();
//			// conn.disconnect();
//		}
//
//		catch (Exception ex)
//		{
//			ex.printStackTrace();
//		}
//
//		if (is != null)
//		{
//			return Drawable.createFromStream(is, "Image");
//		}
//
//		else
//		{
//			Drawable drawable = null;
//			return drawable;
//		}
//	}
    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
