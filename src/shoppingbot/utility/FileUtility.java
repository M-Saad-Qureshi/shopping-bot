/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shoppingbot.utility;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Core i3
 */
public class FileUtility {

    public static String readFile(String filepath) throws FileNotFoundException, IOException {
        StringBuilder content = new StringBuilder();
        String sCurrentLine;
        BufferedReader br = new BufferedReader(new FileReader(filepath));

        while ((sCurrentLine = br.readLine()) != null) {
            content.append(sCurrentLine);
        }

        return content.toString();
    }
}
